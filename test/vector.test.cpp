#include <gtest/gtest.h>
#include <iostream>

#include "../include/Vector/Vector.hpp"
#include <stdexcept>

TEST(VectorCreationTest, CreationTest)
{
    Vector<int> v(2, 1, 2);
    EXPECT_EQ(v.toString(), "(1, 2)");
}

TEST(VectorSetTest, SetTest)
{
    Vector<int> v(2, 1, 2);
    v.set(3, 1, 2, 3);
    EXPECT_EQ(v.toString(), "(1, 2, 3)");
}

TEST(VectorAddTest, AddTest)
{
    Vector<int> v(2, 1, 1);
    Vector<int> w(2, 3, 4);
    v.add(w);
    EXPECT_EQ(v.toString(), "(4, 5)");
}

TEST(VectorAddTest, AddTestError)
{
    Vector<int> v(2, 1, 1);
    Vector<int> w(3, 3, 4, 5);
    EXPECT_THROW(v.add(w),std::length_error);
}

TEST(VectorCross, CrossProduct)
{
    Vector<int> v1(3, 2, 3, 4);
    Vector<int> v2(3, 5, 6, 7);
    Vector<int> res(3, 6, -3);
    EXPECT_EQ(v1.cross(v2), res);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
