NAME=algae
FLAGS=-Wall -Wextra -Werror -pedantic
CC=g++

all: test

test: matrix.test vector.test

matrix.test:
	@echo "Testing Matrix library"
	$(CC) $(FLAGS) -o ./bin/matrix.test ./test/matrix.test.cpp -lgtest -lm
	./bin/matrix.test

vector.test:
	@echo "Testing Vector library"
	$(CC) $(FLAGS) -o ./bin/vector.test ./test/vector.test.cpp -lgtest -lm
	./bin/vector.test

run:
	./bin/main