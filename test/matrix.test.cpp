#include <gtest/gtest.h>
#include <iostream>

#include "../include/Matrix/Matrix.hpp"

TEST(MatrixCreationTest, CreationTest)
{
	Matrix<int> m(2, 2);
	m.set(1, 1, 2);
	m.set(0, 1, 3);
	Matrix<int> m2(2, 2);
	EXPECT_NE(m, m2);
	int data[] = { 0, 0, 3, 2 };
	Matrix<int> m3(data, 2, 2);
	EXPECT_EQ(m, m3);
}

TEST(MatrixCopyTest, CopyTest)
{
	int data[] = { 0, 0, 3, 2 };
	Matrix<int> m(data, 2, 2);
	Matrix<int> m2 = m;
	EXPECT_EQ(m, m2);
}

TEST(MatrixCopyTest, SideEffectTest)
{
	int data[] = { 0, 0, 3, 2 };
	Matrix<int> m(data, 2, 2);
	Matrix<int> m2 = m.copy();
	m.set(1, 1, 4);
	EXPECT_NE(m.toString(), m2.toString());
}

TEST(MatrixDeterminantCalculation, DeterminantTest2x2)
{
	int data[] = { 1, 2, 3, 4 };
	Matrix<int> m(data, 2, 2);
	EXPECT_EQ(m.determinant(), -2);
}

TEST(MatrixDeterminantCalculation, DeterminantTest3x3)
{
	// random data shuffle
	int data[] = { 8, 2, 3, 4, 7, 6, 7, 8, 9 };
	Matrix<int> m(data, 3, 3);
	try {
		EXPECT_EQ(m.determinant(), 81);

	} catch (const char *e) {
		std::cout << e << std::endl;
		EXPECT_TRUE(false);
	}
}

TEST(MatrixMinorTest, GetMinorTest)
{
	int data[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	Matrix<int> m(data, 3, 3);
	Matrix<int> m2 = m.minor(0, 0);
	int data2[] = { 5, 6, 8, 9 };
	Matrix<int> m3(data2, 2, 2);
	EXPECT_EQ(m2, m3);
}

TEST(MatrixInversionTest, InversionTest)
{
	double data[] = { 1., 0, 3, 2 };
	Matrix<double> m(data, 2, 2);
	try {
		Matrix<double> m2 = m.inv();
		double data2[] = { 1, 0, (double)-3 / (double)2,
				   1 / (double)2 };
		Matrix<double> m3(data2, 2, 2);
		std::cout << m2.toString() << std::endl;
		std::cout << m3.toString() << std::endl;
		EXPECT_EQ(m2, m3);
	} catch (const char *e) {
		std::cout << e << std::endl;
		EXPECT_TRUE(false);
	}
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
