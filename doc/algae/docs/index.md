# Algae

A Library for Linear Algebra in C++.

## Features

Vector manipulations.

Matrix operations: addition, subtraction, multiplication, and inversion.

## Installation

```bash
git clone https://framagit.org/1ibre/lib/algae.git
cd algae
```

## Usage

Include the required header (either Vector.hpp or Matrix.hpp) in your project.

```cpp
#include "algae/include/Vector/Vector.hpp"

// ...
Vector<double> v1(3, 1.0, 3.0, 6.0);
// ...
```
